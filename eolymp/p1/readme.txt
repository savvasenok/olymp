https://www.e-olymp.com/en/problems/1

Program reads two-digit number and prints every digit separately, separated by a space.

Input:
23
Output:
2 3

