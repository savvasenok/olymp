https://www.e-olymp.com/ru/problems/2

Find the number of digits in a nonnegative integer n.

Input:
12345
Output:
5

