from math import sqrt
x = float(input())

if abs(x) == 3 or x == 6:
    print('NO FUNCTION')

elif x < -3:
    print('3.0000')

elif -3 < x < 3:
    print('%.4f' % float(3 - sqrt(9-x*x)))

elif 3 < x < 6:
    print('%.4f' % float(-2 * x + 9))

else:
    print('%.4f' % float(x-9))
