#include <iostream>
#include <math.h>
#include <stdio.h>

using namespace std;

int main()
{
    float x;
    scanf("%f", &x);

    if (x == -3.0 || x == 3.0 || x == 6.0)
    {
       puts("NO FUNCTION");
    }
    else if (x < -3.0)
    {
        printf("%4.4f",3.0);
    }
    else if (x > -3.0 && x < 3.0)
    {
        printf("%4.4f", float(3 - sqrt((9)-(x*x))));
    }
    else if (x > 3.0 && x < 6.0)
    {
        printf("%4.4f",float((-2*x) + 9));
    }
    else
    {
        printf("%4.4f",float(x-9));

    }
}
