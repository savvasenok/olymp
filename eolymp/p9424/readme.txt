https://www.e-olymp.com/en/problems/9424

Find the value of the function, given with the graph. The points x = -3, 3, 6 are not included into the domain of the function.

Input:
-5
Output:
3.0000

Input:
-3
Output:
NO FUNCTION
