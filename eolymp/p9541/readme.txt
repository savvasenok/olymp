https://www.e-olymp.com/ru/problems/9541

Chessboard cells of size n*n are numbered consecutive integers from 1 to n*n, 
as shown in the figure. You need to find the sum of the numbers written on the white cells

Input:
4

Output:
68

