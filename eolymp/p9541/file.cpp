#include <iostream>
using namespace std;

int main() {

	long long n;
	int z;
	cin >> n;

	if (n%2 == 1){
		z = ((n+1)/2) * ((n+1)/2) + ((n-1)/2) * ((n-1)/2);
		cout << z*z;
	} else {
		cout << (1 + n*n) * (n/2)*(n/2);
	}
}
